# FireDetector

Detektor pożaru to urzędzenie zbudowane na Raspberry Pi z wykorzystaniem:
- czujnika dymów MQ-2 
- czujnika temperatury i wilgotności DHT11
Połączenie czujników z Raspberry Pi wymagało wykorzystania przetwornika sygnału analogowego na cyfrowy oraz konwertera logicznego napięć.

Skrypt do zbierania danych po stronie Raspberry Pi został napisany w języku Python. Celem skryptu było zebranie danych z czujników, przygotowanie komunikatu w formacie JSON
oraz wysłanie wiadomości przez protokół MQTT do IoT Hub w Azure.
Do kalibracji czujnika dymu wykorzystano gotową bibliotekę: https://github.com/SeeedDocument/wiki_english/blob/master/docs/Grove-Gas_Sensor-MQ2.md

Po stronie Azure'a wykorzystaliśmy narzędzie Streams Analytic, w celu przechwycenia wiadomości z IoT Hub i zapisania ich do bazy SQL Server.
Dane z bazy były wyświetlane w czasie rzeczywistym po stronie aplikacji przygotowanej w języku Python (Flask + JS).

![alt text](https://gitlab.com/tomaszgwozdz/firedetector/raw/master/fire_detector.jpg "Webapp")