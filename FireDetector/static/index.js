var color_range = [
    {
        reference: -30,
        color: {
            red: 20,
            green: 0,
            blue: 255
        }
    }, {
        reference: -27.5,
        color: {
            red: 20,
            green: 0,
            blue: 255
        }
    }, {
        reference: -25,
        color: {
            red: 20,
            green: 0,
            blue: 255
        }
    }, {
        reference: -22.5,
        color: {
            red: 0,
            green: 35,
            blue: 255
        }
    }, {
        reference: -20,
        color: {
            red: 0,
            green: 35,
            blue: 255
        }
    }, {
        reference: -17.5,
        color: {
            red: 0,
            green: 35,
            blue: 255
        }
    }, {
        reference: -15,
        color: {
            red: 0,
            green: 35,
            blue: 255
        }
    }, {
        reference: -12.5,
        color: {
            red: 0,
            green: 35,
            blue: 255
        }
    }, {
        reference: -10,
        color: {
            red: 0,
            green: 60,
            blue: 255
        }
    }, {
        reference: -7.5,
        color: {
            red: 30,
            green: 83,
            blue: 255
        }
    }, {
        reference: -5,
        color: {
            red: 30,
            green: 128,
            blue: 255
        }
    }, {
        reference: -2.5,
        color: {
            red: 68,
            green: 148,
            blue: 253
        }
    }, {
        reference: 0,
        color: {
            red: 95,
            green: 163,
            blue: 253
        }
    }, {
        reference: 2.5,
        color: {
            red: 132,
            green: 185,
            blue: 255
        }
    }, {
        reference: 5,
        color: {
            red: 132,
            green: 209,
            blue: 255
        }
    }, {
        reference: 7.5,
        color: {
            red: 162,
            green: 231,
            blue: 255
        }
    }, {
        reference: 10,
        color: {
            red: 195,
            green: 247,
            blue: 255
        }
    }, {
        reference: 12.5,
        color: {
            red: 225,
            green: 255,
            blue: 252
        }
    }, {
        reference: 15,
        color: {
            red: 255,
            green: 255,
            blue: 150
        }
    }, {
        reference: 17.5,
        color: {
            red: 255,
            green: 255,
            blue: 88
        }
    }, {
        reference: 20,
        color: {
            red: 255,
            green: 250,
            blue: 85
        }
    }, {
        reference: 22.5,
        color: {
            red: 255,
            green: 217,
            blue: 85
        }
    }, {
        reference: 25,
        color: {
            red: 255,
            green: 200,
            blue: 0
        }
    }, {
        reference: 27.5,
        color: {
            red: 238,
            green: 124,
            blue: 60
        }
    }, {
        reference: 30,
        color: {
            red: 241,
            green: 160,
            blue: 0
        }
    }, {
        reference: 32.5,
        color: {
            red: 255,
            green: 130,
            blue: 0
        }
    }, {
        reference: 35,
        color: {
            red: 255,
            green: 100,
            blue: 0
        }
    }, {
        reference: 37.5,
        color: {
            red: 255,
            green: 60,
            blue: 0
        }
    }, {
        reference: 40,
        color: {
            red: 255,
            green: 0,
            blue: 0
        }
    }
];
  
function rgbToHex(r, g, b) {
    var r_hex = r.toString(16);
    var r_hex_val = r_hex.length == 1 ? "0" + r_hex : r_hex;
    var g_hex = g.toString(16);
    var g_hex_val = g_hex.length == 1 ? "0" + g_hex : g_hex;
    var b_hex = b.toString(16);
    var b_hex_val = b_hex.length == 1 ? "0" + b_hex : b_hex;
    
    return "#" + r_hex_val + g_hex_val + b_hex_val;
}


function get_temp(){
    $.ajax({
        type: "get",
        url: "/get_temp",
        success:function(data)
        {
            //console.log the response
            console.log(data);
            $('#inside_temp_block').html(data.inside_temp);
            $('#outside_temp_block').html(data.outside_temp);

            var inside = (Math.round(data.inside_temp* 10) / 10);
            var outside = (Math.round(data.outside_temp * 10) / 10);

            var in_color = 'red';
            var out_color = 'blue';

            var hue;

            for (let index = 0; index < color_range.length; index++) {
                if(inside === color_range[index].reference || (inside <= color_range[index].reference + 2.5 && inside > color_range[index].reference)){
                    in_color=rgbToHex(color_range[index].color.red,color_range[index].color.green,color_range[index].color.blue);
                }
                if(outside === color_range[index].reference || (outside <= color_range[index].reference + 2.5 && outside > color_range[index].reference)){
                    out_color =rgbToHex(color_range[index].color.red,color_range[index].color.green,color_range[index].color.blue);
                }
                
            }
            
            $('.temp_in p').css('border-color', in_color);
    
            $('.temp_out p').css('border-color', out_color);

            setTimeout(function(){
                get_temp();
            }, 5000); //10000 for 10 Seconds
        }
    });
}

// https://gionkunz.github.io/chartist-js/index.html
$(document).ready(function(){
    setTimeout(function(){    
      $('.loader').hide();
      $('.content-temp').show();
      $('.content-switch').show();
    }, 1000);
	
    get_temp();
   
  });
 