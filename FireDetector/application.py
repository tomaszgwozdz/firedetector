import json
import random
import time
import pyodbc
from datetime import datetime
from flask import Flask, Response, render_template, jsonify

application = Flask(__name__)

@application.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"

    return response

@application.route('/get_temp', methods = ['GET'])
def temp():
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=azureprojectserver.database.windows.net;DATABASE=FIreDetector;UID=azureuser;PWD=dAKfsaqA7wgPMuPC')
    cursor = cnxn.cursor()
    cursor.execute("select top 1 Temperature, Humidity from [dbo].data_from_device order by date desc")
    x = cursor.fetchone()
    inside_temp = x[0]
    outside_temp = x[1]
    cursor.close
    cnxn.close()
    return jsonify({"inside_temp":inside_temp, "outside_temp":outside_temp})

@application.route('/', methods =['GET'])
def index():
    inside_temp =  0
    outside_temp =  0

    return render_template('/index.html', inside_temp=inside_temp, outside_temp=outside_temp)

@application.route('/chart-gas')
def chart_gas():
    def generate_random_data():
        while True:
            cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=azureprojectserver.database.windows.net;DATABASE=FIreDetector;UID=azureuser;PWD=dAKfsaqA7wgPMuPC')
            cursor = cnxn.cursor()
            cursor.execute("select top 1 Gas from [dbo].data_from_device order by date desc")
            x = cursor.fetchone()
            x = x[0]
            cursor.close
            cnxn.close()
            json_data = json.dumps(
                {'time': datetime.now().strftime('%H:%M:%S'),
                 'value': int(str(x).split(".")[0])
                 }
            )
            yield f"data:{json_data}\n\n"
            time.sleep(1)

    return Response(generate_random_data(), mimetype='text/event-stream')

@application.route('/chart-co2')
def chart_co2():
    def generate_random_data():
        while True:
            cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=azureprojectserver.database.windows.net;DATABASE=FIreDetector;UID=azureuser;PWD=dAKfsaqA7wgPMuPC')
            cursor = cnxn.cursor()
            cursor.execute("select top 1 Co2 from [dbo].data_from_device order by date desc")
            x = cursor.fetchone()
            x = x[0]
            cursor.close
            cnxn.close()
            json_data = json.dumps(
                {'time': datetime.now().strftime('%H:%M:%S'),
                 'value': int(str(x).split(".")[0])
                 }
            )
            yield f"data:{json_data}\n\n"
            time.sleep(1)

    return Response(generate_random_data(), mimetype='text/event-stream')

@application.route('/chart-smoke')
def chart_smoke():
    def generate_random_data():
        while True:
            cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=azureprojectserver.database.windows.net;DATABASE=FIreDetector;UID=azureuser;PWD=dAKfsaqA7wgPMuPC')
            cursor = cnxn.cursor()
            cursor.execute("select top 1 Smoke from [dbo].data_from_device order by date desc")
            x = cursor.fetchone()
            x = x[0]
            cursor.close
            cnxn.close()
            json_data = json.dumps(
                {'time': datetime.now().strftime('%H:%M:%S'),
                 'value': int(str(x).split(".")[0])
                 }
            )
            yield f"data:{json_data}\n\n"
            time.sleep(1)

    return Response(generate_random_data(), mimetype='text/event-stream')

if __name__ == '__main__':
    application.run(debug=True, threaded=True)