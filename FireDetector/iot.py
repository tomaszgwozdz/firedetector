import time
import board
from mq import *
import sys
import time
from mcp3208 import MCP3208  
import Adafruit_DHT
import RPi.GPIO as GPIO  
from paho.mqtt import client as mqtt
import ssl
import json


adc = MCP3208()
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(27, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(22, GPIO.OUT, initial=GPIO.LOW)


def startScript():
    GPIO.output(18, GPIO.HIGH) 
    time.sleep(2)  
    GPIO.output(18, GPIO.LOW) 

    GPIO.output(27, GPIO.HIGH)  
    time.sleep(2)  
    GPIO.output(27, GPIO.LOW) 

    GPIO.output(22, GPIO.HIGH)  
    time.sleep(2) 
    GPIO.output(22, GPIO.LOW) 


def calibration_on():
    GPIO.output(18, GPIO.HIGH)  
    GPIO.output(27, GPIO.HIGH)  
    GPIO.output(22, GPIO.HIGH) 


def calibration_off():
    GPIO.output(18, GPIO.LOW) 
    GPIO.output(27, GPIO.LOW)  
    GPIO.output(22, GPIO.LOW) 


def azureInit():
    global rootCertPath, deviceId, sasToken, iotHubName, client
    rootCertPath = "/home/pi/Desktop/digicert.cer"
    deviceId = "RaspberryPi"
    sasToken = "SharedAccessSignature sr=project-pi.azure-devices.net%2Fdevices%2FRaspberryPi&sig=zNTJ9DS%2F%2FggZV8IRGMv9KKMMojXJN3soknN%2F6aG0kM8%3D&se=1610579939"
    iotHubName = "project-pi"


def on_connect(client, userdata, flags, rc):
    print("Device connected with result code: " + str(rc))


def on_disconnect(client, userdata, rc):
    print("Device disconnected with result code: " + str(rc))


def on_publish(client, userdata, mid):
    print("Device sent message")


def start_azure_connect():
    global client
    client = mqtt.Client(client_id=deviceId, protocol=mqtt.MQTTv311)

    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_publish = on_publish

    client.username_pw_set(username=iotHubName+".azure-devices.net/" +
                                               deviceId + "/?api-version=2018-06-30", password=sasToken)

    client.tls_set(ca_certs=rootCertPath, certfile=None, keyfile=None,
                   cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
    client.tls_insecure_set(False)

    client.connect(iotHubName+".azure-devices.net", port=8883)


def check_first_sensor():
    try:
        humidity, temperature = Adafruit_DHT.read_retry(11, 15)
        temperatureC = temperature
        humidity = humidity

        print("Temperatura: {:.0f} \xb0C    \nWilgotnosc: {:.0f}% "
              .format(temperatureC, humidity))
        return [temperatureC, humidity]
    except Exception as e:
        print(e)


def check_second_sensor():
    try:
        perc = mq.MQPercentage()
        gas = perc["GAS_LPG"]
        co2 = perc["CO"]
        smoke = perc["SMOKE"]

        print("Stezenie gazu propan-butan: {:.2f} ppm".format(perc["GAS_LPG"]))
        print("Stezenie dwutlenku wegla: {:.2f} ppm".format(perc["CO"]))
        print("Stezenie dymu: {:.2f} ppm".format(perc["SMOKE"]))

        return [gas, co2, smoke]
    except Exception as e:
        print(e)


startScript()
azureInit()
print(rootCertPath, deviceId, sasToken, iotHubName)
start_azure_connect()
calibration_on()
mq = MQ();
calibration_off()
while True:
    data = check_first_sensor()
    temperature = data[0]
    humidity = data[1]

    data = check_second_sensor()
    gas = data[0]
    co2 = data[1]
    smoke = data[2]
    dataDict = {
        'Temperatura': temperature,
        'Wilgotnosc': humidity,
        'PropanButan': gas,
        'DwutlenekWegla': co2,
        'Zadymienie': smoke
    }
    msg = json.dumps(dataDict) 
    print(msg)
    client.publish("devices/" + deviceId + "/messages/events/", msg)
    time.sleep(10)